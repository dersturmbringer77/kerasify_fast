#include "keras_model.h"

// my own assert macro, feel free to use your own or remove all ASSERT(...) calls altogether (not recommended)
#define ASSERT(condition) { if(!(condition)){ std::cerr << "ASSERT FAILED: " << #condition << " @ " << __FILE__ << " (" << __LINE__ << ")" << std::endl; exit(0);} }


template <int N_INPUT, int N1, int N2>
class KerasOptimized
{
public:

	KerasOptimized() : is_initialized(false) {}
	void init(KerasModel& model)
	{
		int i,j;

		KerasLayerDense* layer1 = (KerasLayerDense*)model.layers_[0];
		KerasLayerDense* layer2 = (KerasLayerDense*)model.layers_[1];
		KerasLayerDense* layer3 = (KerasLayerDense*)model.layers_[2];

		ASSERT(model.layers_.size() == 3);

		ASSERT(layer1->weights_.dims_[0] == N_INPUT);
		ASSERT(layer1->weights_.dims_[1] == N1);

		ASSERT(layer2->weights_.dims_[0] == N1);
		ASSERT(layer2->weights_.dims_[1] == N2);

		ASSERT(layer3->weights_.dims_[0] == N2);
		ASSERT(layer3->weights_.dims_[1] == 1);

		// copy weights
		for (i = 0; i < N_INPUT; i++)
			for (j = 0; j < N1; j++)
				weights1[i][j] = layer1->weights_(i,j);
		for (j = 0; j < N1; j++) bias1[j] = layer1->biases_(j);

		for (i = 0; i < N1; i++)
			for (j = 0; j < N2; j++)
				weights2[i][j] = layer2->weights_(i,j);
		for (j = 0; j < N2; j++) bias2[j] = layer2->biases_(j);

		for (i = 0; i < N2; i++)
			weights3[i] = layer3->weights_(i,0);
		bias3 = layer3->biases_(0);

		is_initialized = true;
	}


	// predict model
	// if only_0_1 it will assume inputs can have 0/1 values (big speedup)
	inline float predict(const char* out, bool only_0_1=true) noexcept {
		int i,j;

		ASSERT(is_initialized);

		float res1[N1];
		float res2[N2];
		float res3;

		memcpy(res1, bias1, sizeof(bias1));
		memcpy(res2, bias2, sizeof(bias2));
		res3 = bias3;

		if (only_0_1){
			for (i = 0; i < N_INPUT; i++) {
				if (out[i] == 0) continue;
				for (j = 0; j < N1; j++)
					res1[j] += weights1[i][j];
			}
		} else {
			for (i = 0; i < N_INPUT; i++) {
				if (out[i] == 0) continue;
				float v = out[i];
				float *p_res1 = res1;
				float *p_weights1 = weights1[i];
				if (v == 1){
					for (j = 0; j < N1; j++)
						*p_res1++ += (*p_weights1++);
				} else {
					for (j = 0; j < N1; j++)
						*p_res1++ += (*p_weights1++)*v;
				}
			}
		}

		float *p_res1 = res1;
		for (i = 0; i < N1; i++, p_res1++){
			if (*p_res1 <= 0) continue;
			float* p_res2 = res2;
			float *p_weights2 = weights2[i];
			for (j = 0; j < N2; j++, p_weights2++, p_res2++) 
				*p_res2 += (*p_res1) * (*p_weights2);
		}
		
		float* p_res2 = res2;
		float* p_weights3 = weights3;
		for (i = 0; i < N2; i++, p_res2++, p_weights3++)
			if (*p_res2 > 0)
				res3 += (*p_res2) * (*p_weights3);

		return res3;
	}

protected:
	float weights1[N_INPUT][N1];
	float weights2[N1][N2];
	float weights3[N2];

	float bias1[N1];
	float bias2[N2];
	float bias3;

	bool is_initialized;
};



template <int N_INPUT, int N1, int N2, int N3>
class KerasOptimized_4layers
{
public:

	KerasOptimized_4layers() : is_initialized(false) {}
	void init(KerasModel& model)
	{
		int i,j;

		KerasLayerDense* layer1 = (KerasLayerDense*)model.layers_[0];
		KerasLayerDense* layer2 = (KerasLayerDense*)model.layers_[1];
		KerasLayerDense* layer3 = (KerasLayerDense*)model.layers_[2];
		KerasLayerDense* layer4 = (KerasLayerDense*)model.layers_[3];

		ASSERT(model.layers_.size() == 4);

		ASSERT(layer1->weights_.dims_[0] == N_INPUT);
		ASSERT(layer1->weights_.dims_[1] == N1);

		ASSERT(layer2->weights_.dims_[0] == N1);
		ASSERT(layer2->weights_.dims_[1] == N2);

		ASSERT(layer3->weights_.dims_[0] == N2);
		ASSERT(layer3->weights_.dims_[1] == N3);

		ASSERT(layer4->weights_.dims_[0] == N3);
		ASSERT(layer4->weights_.dims_[1] == 1);

		// copy weights
		for (i = 0; i < N_INPUT; i++)
			for (j = 0; j < N1; j++)
				weights1[i][j] = layer1->weights_(i,j);
		for (j = 0; j < N1; j++) bias1[j] = layer1->biases_(j);

		for (i = 0; i < N1; i++)
			for (j = 0; j < N2; j++)
				weights2[i][j] = layer2->weights_(i,j);
		for (j = 0; j < N2; j++) bias2[j] = layer2->biases_(j);

		for (i = 0; i < N2; i++)
			for (j = 0; j < N3; j++)
				weights3[i][j] = layer3->weights_(i,j);
		for (j = 0; j < N3; j++) bias3[j] = layer3->biases_(j);

		for (i = 0; i < N3; i++)
			weights4[i] = layer4->weights_(i,0);
		bias4 = layer4->biases_(0);

		is_initialized = true;
	}


	inline float predict(const char* out, bool only_0_1=true) noexcept {
		int i,j;

		ASSERT(is_initialized);

		float res1[N1];
		float res2[N2];
		float res3[N3];
		float res4;

		memcpy(res1, bias1, sizeof(bias1));
		memcpy(res2, bias2, sizeof(bias2));
		memcpy(res3, bias3, sizeof(bias3));
		res4 = bias4;

		if (only_0_1)
			for (i = 0; i < N_INPUT; i++){
				if (out[i] == 0) continue;
				float* p_res1 = res1;
				float *p_weights1 = weights1[i];
				for (j = 0; j < N1; j++) 
					(*p_res1++) += *p_weights1++;
			}
		else {
			for (i = 0; i < N_INPUT; i++){
				if (out[i] == 0) continue;
				float v = out[i];
				float *p_res1 = res1;
				float *p_weights1 = weights1[i];
				if (v == 1) {
					for (j = 0; j < N1; j++)
						(*p_res1++) += (*p_weights1++);
				} else {
					for (j = 0; j < N1; j++)
						(*p_res1++) += (*p_weights1++)*v;
				}
			}
		}

		float* p_res1 = res1;
		for (i = 0; i < N1; i++, p_res1++){
			if (*p_res1 <= 0) continue;
			float* p_res2 = res2;
			float* p_weights2 = weights2[i];
			for (j = 0; j < N2; j++, p_res2++, p_weights2++) 
				*p_res2 += (*p_res1) * (*p_weights2);
		}

		float* p_res2 = res2;
		for (i = 0; i < N2; i++, p_res2++){
			if (*p_res2 <= 0) continue;
			float *p_res3 = res3;
			float *p_weights3 = weights3[i];
			for (j = 0; j < N3; j++, p_res3++, p_weights3++) 
				*p_res3 += (*p_res2) * (*p_weights3);
		}

		for (i = 0; i < N3; i++)
			if (res3[i] > 0)
				res4 += res3[i] * weights4[i];

		return res4;
	}

protected:
	float weights1[N_INPUT][N1];
	float weights2[N1][N2];
	float weights3[N2][N3];
	float weights4[N3];

	float bias1[N1];
	float bias2[N2];
	float bias3[N3];
	float bias4;

	bool is_initialized;
};


template <int N_INPUT, int N1, int N2, int N3, int N4>
class KerasOptimized_5layers
{
public:

	KerasOptimized_5layers() : is_initialized(false) {}
	void init(KerasModel& model)
	{
		int i,j;


		KerasLayerDense* layer1 = (KerasLayerDense*)model.layers_[0];
		KerasLayerDense* layer2 = (KerasLayerDense*)model.layers_[1];
		KerasLayerDense* layer3 = (KerasLayerDense*)model.layers_[2];
		KerasLayerDense* layer4 = (KerasLayerDense*)model.layers_[3];
		KerasLayerDense* layer5 = (KerasLayerDense*)model.layers_[4];

		ASSERT(model.layers_.size() == 5);

		ASSERT(layer1->weights_.dims_[0] == N_INPUT);
		ASSERT(layer1->weights_.dims_[1] == N1);

		ASSERT(layer2->weights_.dims_[0] == N1);
		ASSERT(layer2->weights_.dims_[1] == N2);

		ASSERT(layer3->weights_.dims_[0] == N2);
		ASSERT(layer3->weights_.dims_[1] == N3);

		ASSERT(layer4->weights_.dims_[0] == N3);
		ASSERT(layer4->weights_.dims_[1] == N4);

		ASSERT(layer5->weights_.dims_[0] == N4);
		ASSERT(layer5->weights_.dims_[1] == 1);

		// copy weights
		for (i = 0; i < N_INPUT; i++)
			for (j = 0; j < N1; j++)
				weights1[i][j] = layer1->weights_(i,j);
		for (j = 0; j < N1; j++) bias1[j] = layer1->biases_(j);

		for (i = 0; i < N1; i++)
			for (j = 0; j < N2; j++)
				weights2[i][j] = layer2->weights_(i,j);
		for (j = 0; j < N2; j++) bias2[j] = layer2->biases_(j);

		for (i = 0; i < N2; i++)
			for (j = 0; j < N3; j++)
				weights3[i][j] = layer3->weights_(i,j);
		for (j = 0; j < N3; j++) bias3[j] = layer3->biases_(j);

		for (i = 0; i < N3; i++)
			for (j = 0; j < N4; j++)
				weights4[i][j] = layer4->weights_(i,j);
		for (j = 0; j < N4; j++) bias4[j] = layer4->biases_(j);

		for (i = 0; i < N4; i++)
			weights5[i] = layer5->weights_(i,0);
		bias5 = layer5->biases_(0);

		is_initialized = true;
	}


	inline float predict(const char* out, bool only_0_1=true) noexcept{
		int i,j;

		ASSERT(is_initialized);

		float res1[N1];
		float res2[N2];
		float res3[N3];
		float res4[N3];
		float res5;

		memcpy(res1, bias1, sizeof(bias1));
		memcpy(res2, bias2, sizeof(bias2));
		memcpy(res3, bias3, sizeof(bias3));
		memcpy(res4, bias4, sizeof(bias4));
		res5 = bias5;

		if (only_0_1)
			for (i = 0; i < N_INPUT; i++){
				if (out[i] == 0) continue;
				float* p_res1 = res1;
				float *p_weights1 = weights1[i];
				for (j = 0; j < N1; j++) 
					(*p_res1++) += *p_weights1++;
			}
		else {
			for (i = 0; i < N_INPUT; i++){
				if (out[i] == 0) continue;
				float v = out[i];
				float *p_res1 = res1;
				float *p_weights1 = weights1[i];
				if (v == 1) {
					for (j = 0; j < N1; j++)
						(*p_res1++) += (*p_weights1++);
				} else {
					for (j = 0; j < N1; j++)
						(*p_res1++) += (*p_weights1++)*v;
				}
			}
		}

		for (i = 0; i < N1; i++){
			if (res1[i] == 0) continue;
			for (j = 0; j < N2; j++) 
				res2[j] += res1[i] * weights2[i][j];
		}
		for (j = 0; j < N2; j++) 
			if (res2[j] < 0) res2[j] = 0;

		for (i = 0; i < N2; i++){
			if (res2[i] == 0) continue;
			for (j = 0; j < N3; j++) 
				res3[j] += res2[i] * weights3[i][j];
		}
		for (j = 0; j < N3; j++) 
			if (res3[j] < 0) res3[j] = 0;


		for (i = 0; i < N3; i++){
			if (res3[i] == 0) continue;
			for (j = 0; j < N4; j++) 
				res4[j] += res3[i] * weights4[i][j];
		}
		for (j = 0; j < N4; j++) 
			if (res4[j] < 0) res4[j] = 0;

		for (i = 0; i < N4; i++)
			res5 += res4[i] * weights5[i];

		return res5;
	}

protected:
	float weights1[N_INPUT][N1];
	float weights2[N1][N2];
	float weights3[N2][N3];
	float weights4[N3][N4];
	float weights5[N4];

	float bias1[N1];
	float bias2[N2];
	float bias3[N3];
	float bias4[N4];
	float bias5;


	bool is_initialized;
};





template <int N_INPUT, int N1, int N2, int N3>
class KerasOptimizedSoftmax
{
public:

	KerasOptimizedSoftmax() : is_initialized(false) {}
	void init(KerasModel& model)
	{
		int i,j;

		KerasLayerDense* layer1 = (KerasLayerDense*)model.layers_[0];
		KerasLayerDense* layer2 = (KerasLayerDense*)model.layers_[1];
		KerasLayerDense* layer3 = (KerasLayerDense*)model.layers_[2];

		ASSERT(model.layers_.size() == 3);

		ASSERT(layer1->weights_.dims_[0] == N_INPUT);
		ASSERT(layer1->weights_.dims_[1] == N1);

		ASSERT(layer2->weights_.dims_[0] == N1);
		ASSERT(layer2->weights_.dims_[1] == N2);

		ASSERT(layer3->weights_.dims_[0] == N2);
		ASSERT(layer3->weights_.dims_[1] == N3);

		// copy weights
		for (i = 0; i < N_INPUT; i++)
			for (j = 0; j < N1; j++)
				weights1[i][j] = layer1->weights_(i,j);
		for (j = 0; j < N1; j++) bias1[j] = layer1->biases_(j);

		for (i = 0; i < N1; i++)
			for (j = 0; j < N2; j++)
				weights2[i][j] = layer2->weights_(i,j);
		for (j = 0; j < N2; j++) bias2[j] = layer2->biases_(j);

		for (i = 0; i < N2; i++)
			for (j = 0; j < N3; j++)
				weights3[i][j] = layer3->weights_(i,j);
		for (j = 0; j < N3; j++) bias3[j] = layer3->biases_(j);

		is_initialized = true;
	}

	// output is written in array "output"
	inline void const predict(const char* out, float* output) noexcept{
		int i,j;

		ASSERT(is_initialized);

		float res1[N1];
		float res2[N2];
		float res3[N3];

		memcpy(res1, bias1, sizeof(bias1));
		memcpy(res2, bias2, sizeof(bias2));
		memcpy(res3, bias3, sizeof(bias3));

		for (i = 0; i < N_INPUT; i++){
			if (out[i] == 0) continue;
			for (j = 0; j < N1; j++) 
				res1[j] += weights1[i][j];
		}
		for (j = 0; j < N1; j++) 
			if (res1[j] < 0) res1[j] = 0;

		for (i = 0; i < N1; i++){
			if (res1[i] == 0) continue;
			for (j = 0; j < N2; j++) 
				res2[j] += res1[i] * weights2[i][j];
		}
		for (j = 0; j < N2; j++) 
			if (res2[j] < 0) res2[j] = 0;

		for (i = 0; i < N2; i++){
			if (res2[i] == 0) continue;
			for (j = 0; j < N3; j++) 
				res3[j] += res2[i] * weights3[i][j];
		}

		// apply softmax
		float sum = 0;
		float mx = res3[0];

		for (i = 1; i < N3; i++) 
			if (res3[i] > mx) mx = res3[i];
		for (i = 0; i < N3; i++){
			res3[i] = std::exp(res3[i]-mx);
			sum += res3[i];
		}
		for (i = 0; i < N3; i++)
			res3[i] /= sum;

		for (i = 0; i < N3; i++)
			output[i] = res3[i];
	}
	
protected:
	float weights1[N_INPUT][N1];
	float weights2[N1][N2];
	float weights3[N2][N3];

	float bias1[N1];
	float bias2[N2];
	float bias3[N3];

	bool is_initialized;
};




