# kerasify_fast

Faster C++ computations for kerasify library for some basic neural networks

## Description

Kerasify (https://github.com/moof2k/kerasify) is a very handy library to run C++ neural networks generated with Keras. <br>
However in one of my projects I still found it too slow so I wrote my own template classes to make it faster.  <br>
Result is a 5x-10x speedup.  <br>
Not the prettiest code but it is very fast. <br>
Supported network types:  <br>
- 3/4/5 layers and 1 output value
- 4 layers and 1 softmax output tensor 

## Usage

Install Kerasify<br>
Copy keras_optimized.h in the same folder.<br>
Let's say we have trained a 4 layer network in Keras with each layers having 150/64/32/16 elements with inputs only 0/1 values and exported it from python to file my_keras_network.model. Then we can use it like this:

```c++
KerasModel keras_model;
KerasOptimized_4layers<unsigned char, 150,64,32,16> keras_model_fast;

// load 
keras_model.LoadModel("my_keras_network.model", "./");  // load in kerasify
keras_model_fast.init(keras_model);                     // initialize our own network

unsigned char input[150];

// fill input
//[...]

bool input_has_only_0_1_values = true;
float result = keras_model_fast.predict(input, input_has_only_0_1_values);

```

Of course if you have floating point values as input use **float** instead of **unsigned char** <br>
